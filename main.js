const menuIcon = document.querySelector(".menu-icon");
const closeIcon = document.querySelector(".mobile-menu-container .close-icon");
const mobileMenuContainer = document.querySelector(".mobile-menu-container");




const counters = document.querySelectorAll('.count');
const speed = 10;

counters.forEach((counter) => {
  const updateCount = () => {
    const target = parseInt(counter.getAttribute('data-target'));
    const count = parseInt(counter.innerText);
    const increment = Math.trunc(target / speed);

    if (count < target) {
      counter.innerText = count + increment;
      setTimeout(updateCount, 1);
    } else {
      counter.innerText = target;
    }
  };
  updateCount();
});



let slides = document.getElementsByClassName("mySlides");
let authors = document.getElementsByClassName("author");
let prev = document.querySelector(".prev");
let next = document.querySelector(".next");
let dots = document.getElementsByClassName("dot");

if (!slides.length == 0) {
  let slideIndex = 1;
  showSlides(slideIndex);

  function plusSlides(n) {
    showSlides((slideIndex += n));
  }

  let currentSlide = function (n) {
    showSlides((slideIndex = n));
  };

  function showSlides(n) {
    if (n > slides.length) {
      slideIndex = 1;
    }

    if (n < 1) {
      slideIndex = slides.length;
    }

    for (i = 0; i < slides.length; i++) {
      slides[i].style.display = "none";
    }

    

    for (i = 0; i < authors.length; i++) {
      authors[i].style.display = "none";
    }


    for (i = 0; i < dots.length; i++) {
      dots[i].className = dots[i].className.replace(" active", "");
    }

    slides[slideIndex - 1].style.display = "block";

    dots[slideIndex - 1].className += " active";

    authors[slideIndex - 1].style.display = "block";
  }
}

prev.addEventListener("click", () => {
  plusSlides(-1);
});

next.addEventListener("click", () => {
  plusSlides(1);
});


menuIcon.addEventListener("click" , () => {
  mobileMenuContainer.classList.add("open");
});

closeIcon.addEventListener("click", () => {
  mobileMenuContainer.classList.remove("open");
});
